import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Heroe } from '../interfaces/heroe.interface';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeroeService {

  heroesURL = 'https://heroesapp-c3cc7.firebaseio.com/heroes.json';
  heroeURL = 'https://heroesapp-c3cc7.firebaseio.com/heroes';

  constructor( private http: HttpClient ) {}

  nuevoHeroe( heroe: Heroe ) {
    const body = JSON.stringify(heroe);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post( this.heroesURL, body, { headers } )
                     .pipe(map(Response => {console.log(Response);
                      return Response; }));
  }

    actualizarHeroe( heroe: Heroe, key$: string ) {
    const body = JSON.stringify(heroe);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const url = `${ this.heroeURL }/${ key$ }.json`;

    return this.http.put( url , body, { headers } );
  }


  getHeroe( key$: string ) {
        const url = `${ this.heroeURL }/${ key$ }.json`;
        return this.http.get (url);
        return this.http.get(this.heroesURL, {}).pipe( );

  }
  getHeroes(  ) {

        return this.http.get ( this.heroesURL );
        // return this.http.get(this.heroesURL, {}).pipe( );

  }

  borrarHeroe( key$: string ) {
    const url = `${ this.heroeURL }/${ key$ }.json`;
    console.log(url);
    return this.http.delete (url);
  }
}
