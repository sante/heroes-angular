import { Component, OnInit } from '@angular/core';
import { Heroe } from '../../interfaces/heroe.interface';
import { NgForm } from '@angular/forms';
import { HeroeService } from '../../services/heroe.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent implements OnInit {

  private heroe: Heroe = {
    nombre: '',
    bio: '',
    casa: 'Marvel'
  };

  nuevo = false;
  id: string;

  constructor ( private _heroesService: HeroeService,
                private router: Router,
                private route: ActivatedRoute ) {

            this.route.params
            .subscribe( parametros => this.id = parametros ['id']);
            if (this.id !== 'nuevo') {
              this._heroesService.getHeroe(this.id)
              .subscribe( (heroe: Heroe) => this.heroe = heroe );
            }
          }

  ngOnInit() {
  }

  guardar() {
    console.log(this.heroe);
    if (this.id === 'nuevo') {
        // insertando
      this._heroesService.nuevoHeroe(this.heroe)
        .subscribe( (data: any) => {
          this.router.navigate(['/heroe', data.name]);

          // console.log('informacion lista');
          // console.log(data);
        },
            error => console.error(error));
    } else {
    // actualizando
    this._heroesService.actualizarHeroe(this.heroe, this.id )
      .subscribe( data => {
        // console.log('informacion lista');
        console.log(data);

      },
        error => console.error(error));

    }
  }
  agregarNuevo ( forma: NgForm ) {

    this.router.navigate(['/heroe', 'nuevo']);

    forma.reset({
      casa: 'Marvel'
    });
  }

}
