import { Component, OnInit } from '@angular/core';
import { HeroeService } from '../../services/heroe.service';
import { map, timeout } from 'rxjs/operators';
import { pipe } from 'rxjs';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes: any = [] ;
  loading = true;

  constructor ( private _heroeService: HeroeService ) {

    this._heroeService.getHeroes()
      .subscribe( data => {

       // console.log( data );
       // this.loading = false;
       setTimeout(() => {
         this.loading = false;
         this.heroes = data;
       }, 3000);

 /* tslint:disable-next-line:forin
       for (const key$ in data) {
         console.log( data [key$]);
         this.heroes.push( data[key$] );
         // console.log(this.heroes);
        }
        console.log(this.heroes);*/

});
}

  ngOnInit() {
  }

  borraHeroe( key$: string ) {

    this._heroeService.borrarHeroe(key$)
    .subscribe( respuesta => {
      // console.log( respuesta );
      if (respuesta) {
        console.error(respuesta);
      } else {
        // todo bien
        delete this.heroes[key$];
      }
    });
  }
}

